package com.training.springfeignserver;

public class CustomerResponse {

	private int id;
	private String response;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	@Override
	public String toString() {
		return "CustomerResponse [id=" + id + ", response=" + response + "]";
	}
	public CustomerResponse(int id, String response) {
		super();
		this.id = id;
		this.response = response;
	}
	
	
	
}
