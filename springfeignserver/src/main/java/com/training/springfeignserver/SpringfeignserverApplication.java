package com.training.springfeignserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class SpringfeignserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringfeignserverApplication.class, args);
	}

}
