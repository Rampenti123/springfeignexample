package com.training.springfeignserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class FeignController {
	
	@Autowired
	CustomerController controller;

	@PostMapping("/create")
	public CustomerResponse getCustomerData(@RequestBody CustomerRequest customerRequest) {
		return controller.saveCustomer(customerRequest);
	}
}
