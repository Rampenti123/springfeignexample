package com.training.springfeignserver;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;

public class CustomerRequest {
	private int id;
	@NotNull(message = "Name value must needed")
	private String name;
//	@Pattern(regexp = "^[1-9][0-9]{9}$", message = "Invalid phone number format")
	@NotNull(message = "Name value must needed")
	private String phoneNumber;
	@NotNull(message = "Name value must needed")
	private String boughtProductName;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	
	
	public String getBoughtProductName() {
		return boughtProductName;
	}

	public void setBoughtProductName(String boughtProductName) {
		this.boughtProductName = boughtProductName;
	}
	
	
}
