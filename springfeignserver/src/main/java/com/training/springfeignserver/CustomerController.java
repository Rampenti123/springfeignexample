package com.training.springfeignserver;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import jakarta.validation.Valid;

@FeignClient(url="${service.name}" ,value="SERVICE-ONE")
public interface CustomerController {

	@PostMapping(value="/savecustomer",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public CustomerResponse saveCustomer(@RequestBody @Valid CustomerRequest customerRequest) ;
}
