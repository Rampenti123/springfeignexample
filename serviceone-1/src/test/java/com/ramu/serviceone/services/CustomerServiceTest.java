package com.ramu.serviceone.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.ramu.serviceone.dao.CustomerDao;
import com.ramu.serviceone.model.CustomerDataBuilder;
import com.ramu.serviceone.repositories.CustomerRepository;
import com.ramu.serviceone.response.CustomerResponse;
import com.ramu.serviceone.service.CustomerService;
import com.ramu.serviceone.utils.Constants;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CustomerServiceTest {
	

	@Mock
	CustomerDao customerDao;
	@Mock
	CustomerRepository customerRepository;
	@InjectMocks
	CustomerService customerService;

	@Test
	@DisplayName("test createCustomer")
	public void testCreateCustomer() {
		
		when(customerRepository.save(any())).thenReturn(CustomerDataBuilder.getCustomer());
        CustomerResponse saveCustomer = customerService.saveCustomer(CustomerDataBuilder.getCustomerRequest());
        assertEquals(Constants.SUCCESS_MEESSAGE,saveCustomer.getResponse());
	}

}
