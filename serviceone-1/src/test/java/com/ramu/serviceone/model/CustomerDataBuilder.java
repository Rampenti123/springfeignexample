package com.ramu.serviceone.model;

import com.ramu.serviceone.request.CustomerRequest;

public class CustomerDataBuilder {

	public static Customer getCustomer() {
		Customer customer=new Customer();
		customer.setBoughtProductName("Coconut");
		customer.setId(1);
		customer.setName("Harish");
		customer.setPhoneNumber("8178817866");
		return customer;
	}
	
	public static CustomerRequest getCustomerRequest() {
		CustomerRequest customer=new CustomerRequest();
		customer.setBoughtProductName("Coconut");
		customer.setId(1);
		customer.setName("Harish");
		customer.setPhoneNumber("8178817866");
		return customer;
	}
	
}
