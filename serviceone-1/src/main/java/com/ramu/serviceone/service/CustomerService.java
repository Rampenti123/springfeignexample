package com.ramu.serviceone.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ramu.serviceone.controller.CustomerController;
import com.ramu.serviceone.dao.CustomerDao;
import com.ramu.serviceone.model.Customer;
import com.ramu.serviceone.repositories.CustomerRepository;
import com.ramu.serviceone.request.CustomerRequest;
import com.ramu.serviceone.response.CustomerResponse;
import com.ramu.serviceone.utils.Constants;

@Service
public class CustomerService {
	
	private static final Logger LOGGER=LoggerFactory.getLogger(CustomerController.class);
	
	@Autowired
	CustomerRepository customerRepository;

	public CustomerResponse saveCustomer(CustomerRequest customerRequest) {
		LOGGER.info("saveCustomer method is started");
		Customer assignData = CustomerDao.assignData(customerRequest);
		Customer save = customerRepository.save(assignData);
		LOGGER.info("saveCustomer method is ended");
		return new CustomerResponse(save.getId(), Constants.SUCCESS_MEESSAGE);

	}
}
