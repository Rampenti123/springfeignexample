package com.ramu.serviceone.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.ramu.serviceone.request.CustomerRequest;
import com.ramu.serviceone.response.CustomerResponse;
import com.ramu.serviceone.service.CustomerService;
import jakarta.validation.Valid;

@RestController
public class CustomerController {
	
	private static final Logger LOGGER=LoggerFactory.getLogger(CustomerController.class);
	
	@Autowired
	CustomerService customerService;

	@GetMapping(value="/getvalue")
	public String getUser() {
		return "Ramakrishna";

	}
	
	@PostMapping(value="/savecustomer",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public CustomerResponse saveCustomer(@RequestBody @Valid CustomerRequest customerRequest) {
		CustomerResponse saveCustomer = customerService.saveCustomer(customerRequest);
		LOGGER.info("saveCustomer method is ended value"+saveCustomer);
		return saveCustomer;

	}
}
