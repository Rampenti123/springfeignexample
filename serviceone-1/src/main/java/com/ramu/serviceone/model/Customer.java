package com.ramu.serviceone.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Pattern;

@Entity
@Table(name = "customer_tbl")
public class Customer {
	@Id
	private int id;
	private String name;
	private String phoneNumber;
	private String boughtProductName;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", boughtProductName="
				+ boughtProductName + "]";
	}

	public String getBoughtProductName() {
		return boughtProductName;
	}

	public void setBoughtProductName(String boughtProductName) {
		this.boughtProductName = boughtProductName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Customer() {
		super();
	}

}
