package com.ramu.serviceone.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ramu.serviceone.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer>{

}
