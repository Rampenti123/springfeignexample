package com.ramu.serviceone;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class Serviceone1Application {

	public static void main(String[] args) {


		SpringApplication.run(Serviceone1Application.class, args);

	}

}
