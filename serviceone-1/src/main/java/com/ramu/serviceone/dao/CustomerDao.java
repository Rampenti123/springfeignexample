package com.ramu.serviceone.dao;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ramu.serviceone.model.Customer;
import com.ramu.serviceone.repositories.CustomerRepository;
import com.ramu.serviceone.request.CustomerRequest;
import com.ramu.serviceone.response.CustomerResponse;
import com.ramu.serviceone.utils.Constants;
@Component
public class CustomerDao {

	public static Customer assignData(CustomerRequest customerRequest) {
		Customer customer = new Customer();
		BeanUtils.copyProperties(customerRequest, customer);

		return customer;

	}
}
